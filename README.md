# Little tools

This repository contains little tools for the Duniter environment which are too simple to have their own repository.

## Tools index

| Name | Maintainers | Version | Description |
| ---- | ---- | ---- | ---- |
| autocert | tuxmain | 0.0 | (WIP) Renew certifications before they expire (useful for Ğ1Test) |
| keygen | tuxmain | 1.0 | Generate pubkeys from a TSV list of secrets ids |
| natools | tuxmain | 1.3.1 | Encrypt, decrypt, sign, verify using Ed25519 |

## Contribute

Please add, at the top of each file, a licence disclaimer and the maintainers' names with their contacts.

Scripts must not be ambiguous about their interpretor. For example, a bash script must start by something like `#!/usr/bin/bash`; a Python 3 script by `#!/usr/bin/env python3`.

A tool must contain it's own documentation. It should be accessible without any danger by the option `--help`.

Tools must be versioned. A tool's version should be accessible without any danger by the option `--version`. Version numbers should at least be composed of 2 numbers: the first one should be incremented when breaking changes happen. A tool which is not yet usable should have the major version number 0.

A tool containing several files must be in its own directory.
