#!/usr/bin/env python3

"""
Sources:
	CopyLeft 2020 Pascal Engélibert <tuxmain@zettascript.org>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

https://git.duniter.org/clients/python/duniterpy/blob/master/examples
https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/HTTP_API.md
https://git.duniter.org/nodes/typescript/duniter/blob/dev/doc/Protocol.md#protocol-parameters
"""

__version__ = "0.0"

import sys, time, asyncio
import duniterpy.api.bma as bma
from duniterpy.api.client import Client

DEFAULT_BMA_URL = "g1.duniter.fr"
DEFAULT_BMA_PORT = "443"

BMAS_ENDPOINT = "BMAS "+DEFAULT_BMA_URL+" "+DEFAULT_BMA_PORT
IDTY = ""

def getargv(arg, default="", n=1):
	if arg in sys.argv and len(sys.argv) > sys.argv.index(arg)+n:
		return sys.argv[sys.argv.index(arg)+n]
	else:
		return default

async def main():
	client = Client(BMAS_ENDPOINT)
	
	# Get currency parameters
	params = await client(bma.blockchain.parameters)
	
	# Get issued certs
	res = await client(bma.wot.certified_by, IDTY)
	
	# Check if member
	if not res["isMember"]:
		print("Error: "+res["uid"]+" is not member")
	
	# List certs
	for cert in res["certifications"]:
		if cert["isMember"] or True:
			#block = await client(bma.blockchain.block, int(cert["sigDate"].split("-")[0]))
			#expires = block["medianTime"] + params["sigValidity"]
			expires = cert["cert_time"]["medianTime"] + params["sigValidity"]
			print(cert["uid"] + " => " + time.strftime("%d/%m/%Y %H:%M", time.localtime(expires)))
	
	await client.close()

if __name__ == "__main__":
	if "--help" in sys.argv:
		print("""Duniter AutoCert
CopyLeft 2019 Pascal Engélibert
Thanks to the DuniterPy contributors!

Options:
  -e <url> <port>  BMA server address
   default: """+DEFAULT_BMA_URL+" "+DEFAULT_BMA_PORT+"""
  -i <idty>        UID or PubKey
  
  --help           Show help
  --version        Show version
""")
		exit()
	
	if "--version" in sys.argv:
		print(__version__)
		exit()
	
	BMAS_ENDPOINT = "BMAS "+getargv("-e", DEFAULT_BMA_URL, 1)+" "+getargv("-e", DEFAULT_BMA_PORT, 2)
	IDTY = getargv("-i")
	
	asyncio.get_event_loop().run_until_complete(main())
